-- # wp 27
SELECT match_id
    ,start_time
    ,end_time
FROM match;

-- # wp 28
SELECT match_id
    ,game_mode
    ,map_name
FROM match;

-- # wp 29
SELECT *
FROM match;

-- # wp 30
SELECT DISTINCT killer_name
FROM match_frag;

-- # wp 31
SELECT DISTINCT killer_name
FROM match_frag
ORDER BY killer_name;

-- # wp 32
SELECT COUNT(match_id)
FROM match;

-- # wp 33
SELECT COUNT(killer_name) AS kill_suicide_count
FROM match_frag;

-- # wp 34
SELECT COUNT(killer_name) AS suicide_count
FROM match_frag
WHERE victim_name IS NULL;

-- # wp 35
SELECT COUNT(killer_name) AS kill_count
FROM match_frag
WHERE victim_name IS NOT NULL;

-- # wp 36
SELECT COUNT(victim_name) AS kill_count
FROM match_frag;

-- # wp 37
SELECT match_id
    ,COUNT(killer_name) AS kill_suicide_count
FROM match_frag
GROUP BY match_id;

-- # wp 38
SELECT match_id
    ,COUNT(killer_name) AS kill_suicide_count
FROM match_frag
GROUP BY match_id
ORDER BY kill_suicide_count DESC;

-- # wp 39
SELECT match_id
    ,COUNT(killer_name) AS suicide_count
FROM match_frag
WHERE victim_name IS NULL
GROUP BY match_id
ORDER BY suicide_count;

-- # wp 40
SELECT DISTINCT killer_name as player_name
    ,COUNT(killer_name) AS kill_count
FROM match_frag
WHERE victim_name IS NOT NULL
GROUP BY killer_name
ORDER BY kill_count DESC
    ,killer_name;

-- # wp 41
SELECT match_id, killer_name AS player_name
    ,COUNT(killer_name) AS kill_count
FROM match_frag
WHERE victim_name IS NOT NULL
GROUP BY match_id
    ,killer_name
ORDER BY match_id
    ,kill_count DESC
    ,killer_name;

-- # wp 42
SELECT match_id
    ,victim_name AS player_name
    ,COUNT(victim_name) AS death_count
FROM match_frag
WHERE victim_name IS NOT NULL
GROUP BY match_id
    ,player_name
ORDER BY match_id
    ,death_count DESC
    ,player_name;

-- # wp 43
SELECT match.match_id
    ,match.start_time
    ,match.end_time
    ,count(distinct killer_name) as player_count
    ,count(killer_name) as kill_suicide_count
from match
inner join match_frag
    on match.match_id = match_frag.match_id
group by match.match_id
order by match.start_time;


-- # wp 44
-- SQL-01
SELECT match_id, killer_name as player_name, count(killer_name) as kill_count
from match_frag
where victim_name is not null
select count(victim_name) as suicide_count, 0 as deatch_count
from match_frag
where victim_name is null