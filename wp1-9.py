#!/usr/bin/env python3
from os.path import isfile
from datetime import datetime, timedelta, timezone
from csv import writer
from sqlite3 import connect, Error
from argparse import ArgumentParser


def get_input():
    '''
    Take and check the valid file path from input.\
        If the path is not a valid file print error and exit.
        @return: path: a valid file path (str)
    '''

    parser = ArgumentParser(description='The Far Cry project.')
    parser.add_argument('log_file_pathname', action='store',
                        help='the pathname of a Far Cry server log file.')
    args = parser.parse_args()
    return args.log_file_pathname if isfile(args.log_file_pathname)\
        else exit("File not found.")


def read_log_file(log_file_pathname=get_input()):
    try:
        fd = open(log_file_pathname, 'r')
        content = fd.read()
        fd.close()
        return content
    # check input parameter
    except OSError:
        exit('Not a valid log file.')
    except TypeError:
        exit('Invalid parameter(s).')


def convert_to_list(log_data=read_log_file()):
    # convert log data to list
    if isinstance(log_data, str):
        log_data = log_data.splitlines()
    # if param is not string or list, exit.
    elif not isinstance(log_data, list):
        exit('Invalid parameter(s).')
    # return log data as a list
    return log_data


def load_console_variables(log_data=convert_to_list()):
    def get_line_variable(line, console_variables):
        try:
            # get the value in the right side
            line_var = line.split('cvar: ')[1].strip('()')
            key, value = line_var.split(',')
            # change number value to number type if possible
            if '.' in value:
                console_variables[key] = float(value)
            else:
                console_variables[key] = int(value)
        # keep the current format for non-number value
        except ValueError:
            console_variables[key] = value
        except AttributeError:
            exit('Invalid parameter(s).')

    # create and get data for all console variables
    console_variables = {}
    for line in log_data:
        if 'cvar' in line:
            get_line_variable(line, console_variables)
    return console_variables


def parse_log_start_time(log_data, console_variables=load_console_variables()):
    def update_time_with_timezone(time_object, console_variables):
        try:
            # get timezone from console variables
            time_zone = console_variables['g_timezone']
            # create time delta object
            time_delta = timedelta(hours=time_zone)
            # update & return time object with new time zone info
            return time_object.replace(tzinfo=timezone(time_delta))
        except (KeyError, ValueError):
            exit('Timezone not found or invalid.')
        except (NameError, AttributeError):
            exit('Necessary module or function not found.')

    try:
        log_data = convert_to_list(log_data)
        # get the first line
        time_log = log_data[0]
        # convert the string to datetime object
        time_object = datetime.strptime(time_log, 'Log Started at %A, %B %d, %Y %X')
        # update timezone
        time_object = update_time_with_timezone(time_object, console_variables)
        # return object
        return time_object
    except (TypeError, IndexError, ValueError):
        exit('Invalid data.')
    except NameError:
        exit('Necessary module or function not found.')


def parse_session_mode_and_map(log_data):
    try:
        # split raw data into lines
        log_data = convert_to_list(log_data)
        # get the line with mode and map data
        for line in log_data:
            if 'Loading level' in line:
                break
        # split the line properly to get the mode and map
        line_info_list = line.strip('- ').split(' ')
        game_mode = line_info_list[-1]
        for info in line_info_list:
            try:
                game_map = info.split('/')[1].strip(',')
                break
            except IndexError:
                continue
        return (game_mode, game_map)
    except (ValueError, AttributeError):
        exit('Invalid parameter(s).')
    except IndexError:
        exit('Invalid data.')
    except NameError:
        exit('Necessary module or function not found.')


def parse_frags(log_data):
    def get_frag_data(latest_time):
        try:
            frag_list = line.split(' ')
            if line.endswith('killed itself') and 'with' not in frag_list:
                killer = ' '.join(frag_list[2:-2])
                time = update_frag_time(frag_list[0], latest_time)
                latest_time = time
                return (time, killer)
            elif frag_list.count('killed') == 1 and frag_list.count('with') == 1:
                time = update_frag_time(frag_list[0], latest_time)
                latest_time = time
                kill_index = frag_list.index('killed')
                killer = ' '.join(frag_list[2:kill_index])
                with_index = frag_list.index('with')
                victim = ' '.join(frag_list[kill_index+1:with_index])
                weapon = frag_list[-1]
                return (time, killer, victim, weapon)
            else:
                raise ValueError
        except Exception:
            return None

    try:
        start_time, end_time = parse_game_session_start_and_end_times(log_data)
        latest_time = start_time
        # split raw data into lines
        log_data = convert_to_list(log_data)
        frags_list = []
        for line in log_data:
            if 'killed' in line:
                if latest_time > end_time:
                    break
                frag = get_frag_data(latest_time)
                if frag:
                    # add new frag to list
                    frags_list.append(frag)
                    # update latest time
                    latest_time = frag[0]
        return frags_list
    except (AttributeError, ValueError, TypeError):
        exit('Invalid parameter(s).')
    except NameError:
        exit('Necessary module or function not found.')


def update_frag_time(time_string, latest_time):
    try:
        time_string = time_string.strip('<>')
        minutes, seconds = time_string.split(':')
        total_seconds = 60 * int(minutes) + int(seconds)
        total_start_seconds = 60 * latest_time.minute + latest_time.second
        delta = total_seconds - total_start_seconds
        # if total seconds smaller than previous time, add 1 hour
        if delta < 0:
            delta += 3600
        latest_time = latest_time + timedelta(seconds=delta)
        return latest_time
    except (ValueError, TypeError, AttributeError):
        exit('Invalid parameter(s).')
    except NameError:
        exit('Necessary module or function not found.')


def prettify_frags(frags):
    '''
    Single frag: Time + Killer (+ Victim + Weapon)
    '''
    def take_weapon_icon(weapon):
        weapon_dict = {
            '🚙': ['Vehicle'],
            '🔫': ['Falcon', 'Shotgun', 'P90', 'MP5', 'M4','AG36', 'OICW', 
                   'SniperRifle', 'M249', 'MG', 'VehicleMountedAutoMG', 'VehicleMountedMG'],
            '💣': ['HandGrenade', 'AG36Grenade', 'OICWGrenade', 'StickyExplosive'],
            '🚀': ['Rocket', 'VehicleMountedRocketMG', 'VehicleRocket'],
            '🔪': ['Machete'],
            '🚤': ['Boat']
        }
        for key, value in weapon_dict.items():
            if weapon in value:
                return key
        # if weapon not in dict, return the default gun image
        return '🔫'

    try:
        frag_list = []
        for frag in frags:
            frag_string = '[' + frag[0].isoformat() + ']'
            if len(frag) == 2:
                frag_string += ' 😦 %s 💀' % (frag[1])
                frag_list.append(frag_string)
                continue
            # len frag = 4
            weapon_icon = take_weapon_icon(frag[-1])
            frag_string += ' 😛 %s %s 😦 %s' %(frag[1], weapon_icon, frag[2])
            frag_list.append(frag_string)
        return frag_list
    except (AttributeError, IndexError):
        exit('Invalid parameter(s).')


def parse_game_session_start_and_end_times(log_data):
    def get_time_from_line(line, latest_time):
        time_string = line.split(' ')[0]
        latest_time = update_frag_time(time_string, latest_time)
        return latest_time

    try:
        log_data = convert_to_list(log_data)
        for line in log_data:
            if 'loaded in' in line:
                start_line = line
            elif 'Statistics' in line:
                end_line = line
                break

        latest_time = parse_log_start_time(log_data)
        start_time = get_time_from_line(start_line, latest_time)
        end_time = get_time_from_line(end_line, start_time)
        return start_time, end_time
    except UnboundLocalError:
        exit('Start or end time not found in log.')
    except NameError:
        exit('Necessary module or function not found.')


def write_frag_csv_file(frags, log_file_pathname='./log.csv'):
    try:
        log_file = open(log_file_pathname, 'w+')
        log_writer = writer(log_file)
        log_writer.writerows(frags)
        log_file.close()
        print('Successfully added new match data to', log_file_pathname)
    except (OSError, TypeError, ValueError):
        exit('Invalid parameter(s).')


def insert_match_to_sqlite(file_pathname, start_time, end_time, game_mode, map_name, frags, commit=True):
    def insert_frags_to_sqlite(connection, match_id, frags):
        cur = connection.cursor()
        for frag in frags:
            try:
                frag_time, killer, victim, weapon = frag
                frag_time = frag_time.isoformat()
                cur.execute("insert into match_frag(match_id, frag_time, killer_name, victim_name, weapon_code) values (?, ?, ?, ?, ?)", (match_id, frag_time, killer, victim, weapon))
            except ValueError:
                frag_time, killer = frag
                frag_time = frag_time.isoformat()
                cur.execute("insert into match_frag(match_id, frag_time, killer_name) values (?, ?, ?)", (match_id, frag_time, killer))
    try:
        start_time, end_time = start_time.isoformat(), end_time.isoformat()
        conn = connect(file_pathname)
        c = conn.cursor()
        c.execute("insert into match(start_time, end_time, game_mode, map_name) values (?, ?, ?, ?)",
                (start_time, end_time, game_mode, map_name))
        last_id = c.lastrowid
        insert_frags_to_sqlite(conn, last_id, frags)
        if commit:
            conn.commit()
        conn.close()
        print('Successfully added new match data to', file_pathname)
        return last_id
    except (Error, AttributeError, ValueError):
        exit('Invalid parameter(s).')


log_data = read_log_file().splitlines()
log_start_time = parse_log_start_time(log_data)
game_mode, map_name = parse_session_mode_and_map(log_data)
frags = parse_frags(log_data)
# print('\n'.join(prettify_frags(frags)))
start_time, end_time = parse_game_session_start_and_end_times(log_data)
write_frag_csv_file(frags, './log.csv')
file_pathname = './farcry.db'
print(insert_match_to_sqlite(file_pathname, start_time, end_time, game_mode, map_name, frags, False))