#!/usr/bin/env python3
from os.path import isfile
from handling_errors import print_error_and_exit
from datetime import datetime, timedelta, timezone
from csv import writer


def read_log_file(log_file_pathname='./logs/log00.txt'):
    try:
        fd = open(log_file_pathname, 'rb')
        byte_content = fd.read()
        fd.close()
        return byte_content
    # check input parameter
    except OSError:
        print_error_and_exit('not_file')    


def split_data_into_lines(data=read_log_file()):
    try:
        data = data.decode()
        return data.splitlines()
    except (TypeError, UnicodeError):
        print_error_and_exit('data')


def parse_log_start_time(log_data, console_variables):
    try:
        # get the first line
        time_log = log_data[0]
        # convert the string to datetime object
        time_object = datetime.strptime(time_log, 'Log Started at %A, %B %d, %Y %X')
        # update timezone
        time_object = update_time_with_timezone(time_object, console_variables)
        # return object
        return time_object
    except (TypeError, UnicodeError, IndexError):
        print_error_and_exit('data')


def load_console_variables(log_lines=split_data_into_lines()):
    def get_line_variable(line):
        # get the value in the right side
        line_var = line.split('cvar: ')[1].strip('()')
        key, value = line_var.split(',')
        try:
            if '.' in value:
                console_variables[key] = float(value)
            else:
                console_variables[key] = int(value)
        except ValueError:
            console_variables[key] = value

    console_variables = {}
    for line in log_lines:
        if 'cvar' in line:
            get_line_variable(line)
    return console_variables

def update_time_with_timezone(time_object, console_variables):
    try:
        # get timezone from console variables
        time_zone = console_variables['g_timezone']
        # create time delta object
        time_delta = timedelta(hours=time_zone)
        # update & return time object with new time zone info
        return time_object.replace(tzinfo=timezone(time_delta))
    except (KeyError, ValueError):
        print_error_and_exit('data')


def parse_session_mode_and_map(log_data):
    # get the line with mode and map data
    for line in log_data:
        if 'Loading level' in line:
            break
    # split the line properly to get the mode and map
    line_info_list = line.strip('- ').split(' ')
    game_mode = line_info_list[-1]
    for info in line_info_list:
        try:
            game_map = info.split('/')[1].strip(',')
            break
        except IndexError:
            continue
    return (game_mode, game_map)

def parse_frags(log_data, start_time):
    def get_frag_data():
        try:
            time, _, name, _ , victim, _, weapon = line.split(' ')
            # remove extra characters in time
            time = update_frag_time(time, start_time)
            # return frag items
            return (time, name, victim, weapon)
        except ValueError:
            try:
                time, _, name, _, _ = line.split(' ')
                time = update_frag_time(time, start_time)
                return time, name
            except ValueError:
                print_error_and_exit('data')

    # TYPE ERROR, VALUE ERROR
    frags_list = []
    for line in log_data:
        if 'killed' in line:
            frag = get_frag_data()
            frags_list.append(frag)
    return frags_list


def update_frag_time(time_string, start_time):
    # VALUE ERROR, TYPE ERROR
    try:
        time_string = time_string.strip('<>')
        minutes, seconds = time_string.split(':')
        total_seconds = 60 * int(minutes) + int(seconds)
        total_start_seconds = 60 * start_time.minute + start_time.second
        delta = total_seconds - total_start_seconds
        if delta < 0:
            delta += 3600
        return start_time + timedelta(seconds=delta)
    except (ValueError, TypeError):
        print_error_and_exit('data')


def prettify_frags(frags):
    '''
    Single frag: Time + Killer (+ Victim + Weapon)
    '''
    def take_weapon_icon(weapon):
        weapon_dict = {
            '🚙': ['Vehicle'],
            '🔫': ['Falcon', 'Shotgun', 'P90', 'MP5', 'M4','AG36', 'OICW', 
                   'SniperRifle', 'M249', 'MG', 'VehicleMountedAutoMG', 'VehicleMountedMG'],
            '💣': ['HandGrenade', 'AG36Grenade', 'OICWGrenade', 'StickyExplosive'],
            '🚀': ['Rocket', 'VehicleMountedRocketMG', 'VehicleRocket'],
            '🔪': ['Machete'],
            '🚤': ['Boat']
        }
        for key, value in weapon_dict.items():
            if weapon in value:
                return key

    frag_list = []
    for frag in frags:
        frag_string = '[' + frag[0].isoformat() + ']'
        if len(frag) == 2:
            frag_string += ' 😦 %s 💀' % (frag[1])
            frag_list.append(frag_string)
            continue
        # len frag = 4
        weapon_icon = take_weapon_icon(frag[-1])
        frag_string += ' 😛 %s %s 😦 %s' %(frag[1], weapon_icon, frag[2])
        frag_list.append(frag_string)
    return frag_list


def parse_game_session_start_and_end_times(log_data, start_time):
    def get_time_from_line(line):
        time_string = line.split(' ')[0]
        return update_frag_time(time_string, start_time)

    for line in log_data:
        if 'loaded in' in line:
            start_line = line
        elif 'Statistics' in line:
            end_line = line
            break
    try:
        return get_time_from_line(start_line), get_time_from_line(end_line)
    except (NameError, UnboundLocalError):
        print_error_and_exit('data')


def write_frag_csv_file(frags, log_file_pathname='./log.csv'):
    log_file = open(log_file_pathname, 'w+')
    log_writer = writer(log_file)
    log_writer.writerows(frags)
    log_file.close()


log_data = read_log_file()
log_lines = split_data_into_lines()
console_variables = load_console_variables(log_lines)
time_object = parse_log_start_time(log_lines, console_variables)
# print(time_object)
mode_and_map = parse_session_mode_and_map(log_lines)
frags = parse_frags(log_lines, time_object)
# print('\n'.join(prettify_frags(frags)))
start_time, end_time = parse_game_session_start_and_end_times(log_lines, time_object)
# print(str(start_time), str(end_time))
# write_frag_csv_file(frags)