#!/usr/bin/env python3
from sys import exit


def print_error_and_exit(error):
    '''
    Print the error message to stderr and exit with status 1.
        @param: error: error signal (str)
    '''

    # check input
    if not isinstance(error, str):
        print_error_and_exit('param')

    # error dict
    error_messages = {
        'not_dir': 'Not a valid directory.',
        'not_file': 'Not a valid file.',
        'param': 'Invalid parameter(s).',
        'data': 'Invalid data.'
    }
    # print the error
    if error in error_messages:
        print(error_messages[error])
    else:
        print('Unidentified error occurs.')
    # exit
    exit(1)
